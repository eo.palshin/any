### Количество задействованных аудиторий в расписании занятий весеннего семестра
---
| Корпус | Количество аудиторий|
| --- | ---: |
| кафедра | 13 |
| адм | 39 |
| 1 | 24 |
| 3 | 52 |
| 4 | 3 |
| 5 | 109 |
| 6 | 4 |
| 7 | 23 |
| 8 | 1 |
| 10 | 13 |
| 11 | 5 |
| 12 | 1 |
| 14 | 75 |
| 15 | 11 |
| 18 | 20 |
| 22 | 37 |
| 22а | 42 |
| 22б | 49 |
| 22в | 7 |
| 23 | 19 |
| 24 | 43 |
| 25 | 1 |
| 27 | 39 |

```sql
SELECT timetable_buildings.name, COUNT(DISTINCT timetable_rooms.name) FROM timetable_lessons
INNER JOIN timetable_semesters
  ON timetable_lessons.timetable_semester_id = timetable_semesters.id
INNER JOIN timetable_academic_years
  ON timetable_semesters.timetable_academic_year_id = timetable_academic_years.id
INNER JOIN timetable_lesson_weeks
  ON timetable_lesson_weeks.timetable_lesson_id = timetable_lessons.id
INNER JOIN timetable_rooms 
  ON timetable_rooms.id = timetable_lesson_weeks.room_id
INNER JOIN timetable_buildings
  ON timetable_buildings.id = timetable_rooms.timetable_building_id
WHERE
timetable_semesters.semester_type = 2
AND timetable_academic_years.name = '2019-2020'
GROUP BY timetable_buildings.name
ORDER BY timetable_buildings.name + 0
```

